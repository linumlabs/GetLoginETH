import { Contract } from 'ethers';
import { ethers } from 'hardhat';
import GetLoginLogic from "../artifacts/contracts/GetLoginLogic.sol/GetLoginLogic.json";
import { Wallet } from './interface';

async function main() {
  const [deployer] = await ethers.getSigners();
  // tslint:disable-next-line: no-console
  console.log(
      "Deploying contracts with the account:",
      deployer.address,
    );

    const loginStorage = await ethers.getContractFactory("GetLoginStorage");
    const loginLogic = await ethers.getContractFactory("GetLoginLogic");

    var loginStorageContract = await loginStorage.deploy();
    var loginLogicContract = await loginLogic.deploy(loginStorageContract.address);

    console.log("\nStorage address:", loginStorageContract.address);
    console.log("\nLogic address:", loginLogicContract.address);

    await(await loginStorageContract.setLogicAddress(loginLogicContract.address)).wait();

    console.log("\nLogic Address Set!");

    await(await loginLogicContract.init()).wait();
    console.log("\nLogic Contract Init!");
    console.log('\n\n-------creating user---------')

    var randomWallet = ethers.Wallet.createRandom();
    // @ts-ignore
    const json = await randomWallet.encrypt("test")
    const data:Wallet = JSON.parse(json);
    
   
    var formattedAddress = "0x" + data.address
    console.log('\n ADDRESS CREATED :'  + formattedAddress)
    console.log('\n INVITE :'  + randomWallet.privateKey)

    await(await loginLogicContract.createInvite([formattedAddress], {from:deployer.address,  value: ethers.utils.parseEther("3")})).wait()


    await(await deployer.sendTransaction({from:deployer.address, to:formattedAddress, value: ethers.utils.parseEther("1") })).wait()
    console.log('\n\n-------Deployer invites---------')

    console.log(await loginStorageContract.getInvite(formattedAddress));

    console.log(`\nINVITE CREATED for ${formattedAddress}`);
  }

  main()
    .then(() => process.exit(0))
    .catch(error => {
      // tslint:disable-next-line: no-console
      console.error(error);
      process.exit(1);
    });
