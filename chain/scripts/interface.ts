export interface Cipherparams {
    iv: string;
}

export interface Kdfparams {
    salt: string;
    n: number;
    dklen: number;
    p: number;
    r: number;
}

export interface Crypto {
    cipher: string;
    cipherparams: Cipherparams;
    ciphertext: string;
    kdf: string;
    kdfparams: Kdfparams;
    mac: string;
}

export interface XEthers {
    client: string;
    gethFilename: string;
    mnemonicCounter: string;
    mnemonicCiphertext: string;
    path: string;
    locale: string;
    version: string;
}

export interface Wallet {
    address: string;
    id: string;
    version: number;
    Crypto: Crypto;
}



