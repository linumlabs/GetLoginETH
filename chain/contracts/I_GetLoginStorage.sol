pragma solidity 0.6.4;
pragma experimental ABIEncoderV2;

abstract contract I_GetLoginStorage {
    struct Username {
        bool isActive;
        // todo define a uniform variable name
        bytes32 username;
    }

    struct UserInfo {
        // todo define a uniform variable name
        bytes32 username;
        bool isActive;
    }

    struct InviteInfo {
        address inviteAddress;
        // todo define a uniform variable name
        bytes32 creatorUsername;
        bytes32 registeredUsername;
        bool isActive;
    }

    struct UserSession {
        // todo define a uniform variable name
        bytes32 username;
        address wallet;
        uint8 sessionType;
        uint64 appId;
    }

    struct Application {
        uint64 id;
        // todo define a uniform variable name (usernameHash or username)
        bytes32 usernameHash;
        string title;
        string description;
        string[] allowedUrls;
        address[] allowedContracts;
        bool isActive;
    }

    //--------------------------------------------------------------------------
    // View functions
    //--------------------------------------------------------------------------

    function getUser(bytes32 usernameHash)
        public
        view
        virtual
        returns (UserInfo memory);

    function getOwner() public view virtual returns (address);

    function getLogicAddress() public view virtual returns (address);

    function getApplicationId() public view virtual returns (uint64);

    function getSettings(bytes32 key)
        public
        view
        virtual
        returns (string memory);

    function getUsersAddressUsername(address _address)
        public
        view
        virtual
        returns (Username memory);

    function getInvite(address _address)
        public
        view
        virtual
        returns (InviteInfo memory);

    function getUserSessions(bytes32 usernameHash)
        public
        view
        virtual
        returns (UserSession[] memory);

    function getApplication(uint64 id)
        public
        view
        virtual
        returns (Application memory);

    //--------------------------------------------------------------------------
    // Public functions
    //--------------------------------------------------------------------------

    function setLogicAddress(address _address) public virtual;

    function setUser(bytes32 usernameHash, UserInfo memory info) public virtual;

    function setSettings(bytes32 key, string memory value) public virtual;

    function setUsersAddressUsername(address _address, Username memory info)
        public
        virtual;

    function setApplication(uint64 id, Application memory data) public virtual;

    function incrementApplicationId() public virtual;

    function pushApplicationUrl(uint64 id, string memory url) public virtual;

    function pushApplicationContract(uint64 id, address wallet) public virtual;

    function deleteApplicationUrl(uint64 id, uint256 index) public virtual;

    function deleteApplicationContract(uint64 id, uint256 index) public virtual;

    function pushUserSession(
        bytes32 usernameHash,
        address wallet,
        uint8 sessionType,
        uint64 appId
    ) public virtual;

    function setInvite(address _address, InviteInfo memory data) public virtual;

    function emitEventStoreWallet(
        bytes32 username,
        address walletAddress,
        string memory ciphertext,
        string memory iv,
        string memory salt,
        string memory mac
    ) public virtual;

    function emitEventInviteCreated(
        bytes32 creatorUsername,
        address inviteAddress
    ) public virtual;

    function emitEventAppSession(
        uint64 appId,
        bytes32 username,
        string memory iv,
        string memory ephemPublicKey,
        string memory ciphertext,
        string memory mac
    ) public virtual;

    function emitEventAppCreated(bytes32 creatorUsername, uint64 appId)
        public
        virtual;
}
