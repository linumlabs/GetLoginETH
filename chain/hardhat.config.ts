import { task } from "hardhat/config";
import "hardhat-contract-sizer";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import { HardhatUserConfig } from 'hardhat/types';
import * as dotenv from "dotenv";

dotenv.config();


/**
 * @type import('hardhat/config').HardhatUserConfig
 */

const config: HardhatUserConfig = {
  defaultNetwork: "localhost",
  solidity: {
    compilers: [{ version: "0.6.4", settings: { optimizer: {
      enabled: true,
      runs: 400
    },} }],
  },
  networks: {
    localhost: {
      url: "http://localhost:8545",
      chainId: 31337,
      allowUnlimitedContractSize: true
    },
    goerli: {
      url: `https://goerli.infura.io/v3/${process.env.INFURA_PROJECT_ID}`,
      accounts: [`0x${process.env.GOERLI_PRIVATE_KEY}`],
      chainId: 5,
    },
  },
  // mocha options can be set here
  mocha: {
    // timeout: "300s",
  },
};
export default config;