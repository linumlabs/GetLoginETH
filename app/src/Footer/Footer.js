import React from "react";
import "./Footer.css";
import { Link } from "react-router-dom";

function Footer({ buildDate }) {
  return (
    <footer className="footer Footer text-center text-sm-left">
      <span className="text-muted Footer-text-container">
        © 2020 Company, Inc. ·{" "}
        <Link className="Footer-link" to="./privacy">
          Privacy
        </Link>{" "}
        ·{" "}
        <Link className="Footer-link" to="./terms">
          Terms
        </Link>{" "}
        ·{" "}
        <Link className="Footer-link" to="./developers">
          Developers
        </Link>{" "}
        ·{" "}
        <a
          className="Footer-link"
          href="https://github.com/GetLoginEth/login"
          target="_blank"
          rel="noopener noreferrer"
        >
          GitHub
        </a>
      </span>
      <span className="Footer-build mx-sm-3 float-sm-right text-muted">
        Build date: {buildDate}
      </span>
    </footer>
  );
}

export default Footer;
