export const config = {
    rinkeby: {
        websocketProviderUrl:`${process.env.REACT_APP_INFURA_ENDPOINT_HTTP}`,
        contractAddress: `${process.env.REACT_APP_STORAGE_ADDRESS}`,
        isTrezorEnabled: false
    },
    goerli: {
        contractAddress: '0x7198DF5d9DBb7889EE5d91c4B3dB88bE89492958',
        websocketProviderUrl:`${process.env.REACT_APP_INFURA_ENDPOINT_HTTP}`,
        isTrezorEnabled: false
    },
    mainnet: {
        // todo set mainnet contract
        contractAddress: '',
        websocketProviderUrl: 'wss://mainnet.infura.io/ws/v3/357ce0ddb3ef426ba0bc727a3c64c873',
        isTrezorEnabled: false
    },
    localhost: {
        // todo set mainnet contract
        contractAddress: `${process.env.REACT_APP_STORAGE_ADDRESS}`,
        websocketProviderUrl: 'http://localhost:8545',
        isTrezorEnabled: false
    }
};

export function getConfig(network) {
    if (!config[network]) {
        throw new Error(`Config for this network not found: ${network}`);
    }

    return config[network];
}
