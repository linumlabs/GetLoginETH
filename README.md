## GetLogin

### **Contents**

- [Description](#description)
- [Daniel Nagy's EthCC talk](#daniel-nagy-on-getlogin-at-ethcc3)
- [Getting Started With This Repo](#getting-started-with-this-repo)
- [Register an App](#register-your-app)
- [Inject Into Your Dapp](#inject-into-your-dapp)
- [Built-in Methods](#built-in-methods)
- [Call dApp contract methods](#call-dapp-contract-methods)
- [Donations](#donations)


### Description

GetLogin is a Đapp for providing a decentralized single sign-on solution for Ethereum-based social applications with a familiar (Web2.0-style) onboarding experience that does not require ether or even any experience with Ethereum.

Initially the user's account is protected by a user name and password, but as the value of the account grows and the user becomes more familiar with Ethereum, it is possible to use a better-protected Ethereum address (e.g. that of a hardware wallet) as the source of login credentials.

GetLogin comes with an invitation system. The gas costs of new users are covered by those who invited them and the latter are also their first point of contact for social account recovery in the case of lost or compromised credentials.

GetLogin implements an [OpenID Connect](https://openid.net/) authentication through [OAuth 2](https://en.wikipedia.org/wiki/OAuth#OAuth_2.0) ([implicit flow](https://openid.net/specs/openid-connect-core-1_0.html#ImplicitFlowAuth) - client-side only).

Last project build hosted at [getlogin.eth](https://swarm-gateways.net/bzz:/getlogin.eth/).

### Daniel Nagy on GetLogin at EthCC[3]

[![ETHCC2020 - Daniel Nagy: "Decentralized User Account Management"](http://i3.ytimg.com/vi/vX3F4QyQRw8/maxresdefault.jpg)](https://www.youtube.com/watch?v=vX3F4QyQRw8)

### Getting Started With This Repo

This repository contains two sub-repositories. `app` conatains the logic for the frontend (which is based on `create-react-app`), and `chain` contains a blockchain environment (using [Hardhat](https://hardhat.io)).

In order to run this repo locally, first clone the repo and run `yarn install` in both the `app` and `chain` directories separately. You will then need three open terminals:

1. In the first, run `yarn start` from the `chain` directory. This will start a local dev chain using Hardhat. Keep this terminal open
2. In the second, run `yarn deploy:local`. This will deploy the relevant contracts to the local chain, and also provide information you will need in order to register your account.
3. From the `app` directory, run `yarn start:unix` if you are on Linux, MacOS, or similar, or `yarn start:windows` if you are on Windows. This will start the frontend on port 3001. You may be shown warnings that there are no valid https certificates - these can be ignored.

This should give you a working frontend in your browser.

In order to register an account, click on "Get Started" or "Sign up". (url: https://localhost:3001/xsignup). Fill in username and password, then for invite refer to the terminal used to deploy the contracts. There should be a field starting "INVITE CREATED" followed by a hex. Copy the hex (**without** the `0x`, just everything after it), and paste it into the invite field in the app. This should be enough to generate the account and log in!

There are also scripts provided for deploying to Rinkeby or mainnet, though they have not been made OS-specific, and should be inspected in the `package.json` for more details if necessary.

### Register your app

1) Receive an invite (you can write to my email: igor.shadurin@gmail.com) and register account.
2) Use page to manage your application data: https://getlogin.swarm-gateways.net/

### Inject into your dApp

Add the following code before the footer: 

```javascript
window._onGetLoginApiLoaded = (instance) => {
    window.getLoginApi = instance;
    instance.init(appId, 'https://getlogin.swarm-gateways.net/', redirectUrl, accessToken)
    .then(data => {
        console.log(data);
    });
}
``` 
where `appId` is your app id can be found in your application data page (see step 2 of "Register Your App"), and `redirectUrl` is your app url.

`accessToken` is the access token which you received early or `null`

Add `<script src="https://getlogin.swarm-gateways.net/api/last.js"></script>` to footer.
    
After loading the script, it will call the `window._onGetLoginApiLoaded` method and pass the GetLogin instance to it.                     
                           
### Built-in methods

```javascript
window.getLoginApi.getUserInfo()
    .then(data => alert(JSON.stringify(data)))
    .catch(e => alert(e));
```

`getUserInfo()` - get current user information

`isReady()` - check is iframe ready

`setClientAbi(abi)` - set your dApp ABI

`getClientAbi()` - get current ABI

`getAuthorizeUrl()` - get URL for authorize user in you dApp

`resetInit()` - reset iframe

`async init(appId, baseApiUrl, redirectUrl, accessToken = null)` - init GetLogin. This promise returns object `{authorize_url: string, is_client_allowed: boolean, client_id: int, type: "get_login_init"}`. `is_client_allowed` - is user authorized your dApp, if false - show `authorize_url` for authorization.

`logout()` - logout user from your dApp

`async callContractMethod(address, method, ...params)` - call dApp contract method (read-only)

`async sendTransaction(address, method, txParams, params)` - send transaction to your dApp contract

`async setOnLogout(func)` - set callback when user logged out

`async keccak256(data)` - get keccak256 hash from passed data

`async getPastEvents(address, eventName, params)` - get events from contract

`async getAccessTokenBalance()` - receive balance on access token

### Call dApp contract methods

Set contract ABI once before calling dApp methods: `window.getLoginApi.setClientAbi(abi);`

Call getNotes method which defined in your dapp contract: 

```javascript
window.getLoginApi.callContractMethod(address, 'getNotes', usernameHash)
.then(data => {
    console.log(data);
})
.catch(e => {
    console.log(e);
});
```

`address` is your dapp address

Send transaction to your dapp contract: 

```javascript
window.getLoginApi.sendTransaction(address, 'createNote', [noteText], {resolveMethod: 'mined'})
.then(data => {
    console.log(data);
})
.catch(e => {
    console.log(e);
});
```

`address` is your dapp address.

`createNote` is your dapp method defined in contract.

`noteText` content of your note.
                                                     
One important param is `resolveMethod`. Values of this param can be: 

`mined` - fired when tx sent and mined (slow)

`transactionHash` - when tx sent (fast)

### Donations

Donations accepted by ETH address getlogin.eth or 0xde442ceD045ae30e076597C428876782b42D24cC
